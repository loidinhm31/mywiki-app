package com.july.wikipedia.models

class WikiPage {
    // Name of variable must equal exactly the key in JSON file
    var pageid: Int?= null
    var title: String?= null
    var fullurl: String?= null
    var thumbnail: WikiThumbnail?= null
}